﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column : MonoBehaviour {

    // Army control
    public static float armySpeed = 5;
    private static float armydirection = 1;
    private float direction = 1;
    private float speed;

    // Reference to each alien in column
    public Alien alien1Ref;
    public Alien alien2Ref;
    public Alien alien3Ref;
    public Alien alien4Ref;

    // Fire control variables
    public float baseFireWaitTime;
    public float minFireRateTime;
    public float maxFireRateTime;

    // Inavade Boolean
    private bool invade;

    // Use this for initialization
    void Start () {
        invade = true;
        speed = armySpeed;
        GetComponent<Rigidbody2D>().velocity = new Vector2(armydirection, 0) * speed;
        baseFireWaitTime = Random.Range(minFireRateTime, maxFireRateTime);
    }

    // Cease invasion 
    public void Stop() {
        invade = false;
        armySpeed = 0;
    }

    // Update for rigidBody2D
    void FixedUpdate () {
        if(armydirection != direction) {
            MoveDown();
            direction = armydirection;
            GetComponent<Rigidbody2D>().velocity = new Vector2(direction, 0) * speed;
        }

        if(armySpeed != speed) {
            speed = armySpeed;
            GetComponent<Rigidbody2D>().velocity = new Vector2(direction, 0) * speed;
            if (gameObject.transform.childCount == 0) Destroy(gameObject);
        }
        
    }

    // Give command to switch army directin on wall contact
    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "Wall") {
            armydirection = armydirection * -1;
        }
    }

    // Moves the Alien vertically down
    void MoveDown() {
        if (invade) {
        Vector2 position = transform.position;
        position.y -= 3;
        transform.position = position;
        }
    }

    // Control Column fire
    void Bomb() {
        if (alien4Ref != null && invade) alien4Ref.Shoot();
        else if (alien2Ref != null && invade) alien2Ref.Shoot();
        else if (alien1Ref != null && invade) alien1Ref.Shoot();
        else if (alien3Ref != null && invade) alien3Ref.Shoot();
    }

    // Update is called once per frame
    void Update () {

        // Calculate whether to drop bomb
        if (Time.time > baseFireWaitTime) {
            baseFireWaitTime = baseFireWaitTime + Random.Range(minFireRateTime, maxFireRateTime);
            Bomb();

        }
    }
}
