﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {

    // Movement speed
    public float speed = 30;

    // Bullet object
    public Bullet theBullet;

    // Death sprite
    public DeathSprite shipDeath;

    // Gameover splash
    public Splash gameOverPrefab;

    // FixedUpdate called to update rigid body
    void FixedUpdate() {

        // Player move control
        float horizontal_dir = Input.GetAxisRaw("Horizontal");
        GetComponent<Rigidbody2D>().velocity = new Vector2(horizontal_dir, 0) * speed;
    }

    // Kill ship function
    public void kill() {
        Instantiate(shipDeath, transform.position, Quaternion.identity);
        Instantiate(gameOverPrefab);
        Destroy(gameObject);
    }

    // On collision
    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Bomb") {
            Destroy(col.gameObject);
            kill();
        }
    }


    // Use this for initialization
    void Start() {
		
	}

    // Update is called once per frame
    void Update() {

        // Player fire control
        if (Input.GetButtonDown("Jump")) {

            // Create a Bullet at transform.position which
            // is the ships current location
            // Quaternion.identity adds Bullet with no rotation
            Instantiate(theBullet, transform.position, Quaternion.identity);

        }
    }
}
