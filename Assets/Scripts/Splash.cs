﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splash : MonoBehaviour {

    // Keep track of singleton instance
    private static Splash instance = null;

	// Use this for initialization
	void Start () {

        //Destroy if one already exists
        if (instance == null) instance = this;
        else Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
