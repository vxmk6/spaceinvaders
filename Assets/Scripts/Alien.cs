﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien : MonoBehaviour {

    public DeathSprite alientDeath;

    // Speed increase on death
    public float increaseOnDeath = 0;

    // Bomb prefab
    public Bomb bombPrefab;

    // Column parent
    public Column thisColumn;

    // Switch direction on collision
    void OnTriggerEnter2D(Collider2D col) {
        
        if (col.gameObject.tag == "Bullet") {
            Destroy(col.gameObject);
            Column.armySpeed += increaseOnDeath;
            Instantiate(alientDeath, transform.position, Quaternion.identity);
            Destroy(gameObject);
            
        }
    }

    // Shoot bomb
    public void Shoot() {
        Instantiate(bombPrefab, transform.position, Quaternion.identity);
    }

    // Use this for initialization
    void Start () {

    }

    // Update for rigidbody2D
    void FixedUpdate() {

    }

    // Update is called once per frame
    void Update () {
        
    }
}
