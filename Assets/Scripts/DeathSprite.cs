﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSprite : MonoBehaviour {

	//Corpse linger time
	public float corpseLingerTime = 0.05f;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, corpseLingerTime);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
