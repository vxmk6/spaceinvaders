﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

    //Speed variable
    public float speed = 30;

    // Use this for initialization
    void Start() {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, -1) * speed;
    }

    // Destroy on collision
    void OnTriggerEnter2D(Collider2D col) {
        if ( col.gameObject.tag == "Wall") {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update() {

    }
}

