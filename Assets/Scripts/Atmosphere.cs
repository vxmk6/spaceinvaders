﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atmosphere : MonoBehaviour {

	// Reference to ship
	public Ship shipReference;

	// Alien enters atmosphere
	private void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Alien"){
			if (shipReference != null) shipReference.kill();
            gameObject.GetComponent<SpriteRenderer>().color = Color.green;
            other.GetComponent<Alien>().thisColumn.Stop();
        }
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
}
