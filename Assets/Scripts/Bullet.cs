﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    // Speed variable
    public float speed = 30;

    // Singleton instnace
    private static Bullet instance = null;

	// Use this for initialization
	void Start () {

        // If the is no other bullet, send bullet upwards
        if (instance == null) {
            instance = this;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1) * speed;
        }

        // There is a bullet already, destroy this one
        else Destroy(gameObject);

    }

    // Collision event
    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Wall") {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
